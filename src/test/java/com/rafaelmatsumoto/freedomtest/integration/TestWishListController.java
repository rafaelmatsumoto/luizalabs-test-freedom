package com.rafaelmatsumoto.freedomtest.integration;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rafaelmatsumoto.freedomtest.models.ApiResponse;
import com.rafaelmatsumoto.freedomtest.repository.WishListRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class TestWishListController {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private WishListRepository wishListRepository;

    private static final String API_PREFIX = "/api/v1/wishlist/";

    @Test
    void canRegisterNewProduct() throws Exception {
        UUID userId = UUID.randomUUID();
        ResultActions resultActions = mockMvc.perform(
                post(API_PREFIX + userId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "\t\"id\": \"knife-4\",\n" +
                                "\t\"name\": \"knife\",\n" +
                                "\t\"price\": \"3.6\",\n" +
                                "\t\"quantity\": 2\n" +
                                "}")
        );
        resultActions.andExpect(status().isOk());
    }

    @Test
    void canDeleteProduct() throws Exception {
        UUID userId = UUID.randomUUID();
        UUID productId = UUID.randomUUID();
        mockMvc.perform(
                post(API_PREFIX + userId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "\t\"id\": " + productId + ",\n" +
                                "\t\"name\": \"knife\",\n" +
                                "\t\"price\": \"3.6\",\n" +
                                "\t\"quantity\": 2\n" +
                                "}")
        );

        ResultActions resultActions = mockMvc.perform(
                delete(API_PREFIX + userId + "/" + productId)
        );

    }

    @Test
    void canGetProductInWishlist() throws Exception {
        UUID userId = UUID.randomUUID();
        UUID productId = UUID.randomUUID();
        mockMvc.perform(
                post(API_PREFIX + userId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "\t\"id\": " + productId + ",\n" +
                                "\t\"name\": \"knife\",\n" +
                                "\t\"price\": \"3.6\",\n" +
                                "\t\"quantity\": 2\n" +
                                "}")
        );
        ResultActions resultActions = mockMvc.perform(
                get(API_PREFIX + userId + "/" + productId)
        );
        String productResult = resultActions.andReturn().getResponse().getContentAsString();
        ApiResponse apiResponse = objectMapper.readValue(
                productResult,
                new TypeReference<>() {
                }
        );
        assertThat(apiResponse.getMessage()).isEqualTo("Product found");
    }

    @Test
    void canGetAllProductsFromWishlist() throws Exception {
        UUID userId = UUID.randomUUID();
        UUID productId = UUID.randomUUID();
        mockMvc.perform(
                post(API_PREFIX + userId)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "\t\"id\": " + productId + ",\n" +
                                "\t\"name\": \"knife\",\n" +
                                "\t\"price\": \"3.6\",\n" +
                                "\t\"quantity\": 2\n" +
                                "}")
        );
//        System.out.println(wishListRepository.findById(String.valueOf(userId)));
        ResultActions resultActions = mockMvc.perform(
                get(API_PREFIX + userId)
        );
        String getProductsResult = resultActions.andReturn().getResponse().getContentAsString();
        ApiResponse apiResponse = objectMapper.readValue(
                getProductsResult,
                new TypeReference<>() {
                }
        );
        assertThat(apiResponse.getStatus()).isEqualTo("Success");
    }

}
