package com.rafaelmatsumoto.freedomtest.models;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

import java.io.Serializable;
import java.util.List;

@RedisHash
@Data
public class WishList implements Serializable {
    @Id
    private String userId;

    private List<Product> products;
}
