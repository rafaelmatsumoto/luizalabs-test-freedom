package com.rafaelmatsumoto.freedomtest.models;

import lombok.Data;

@Data
public class ApiResponse {
    private String message;

    private String status;

    private WishList data;
}
