package com.rafaelmatsumoto.freedomtest.models;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

import java.io.Serializable;

@RedisHash
@Data
public class Product implements Serializable {
    @Id
    private String id;

    private String name;

    private Float price;

    private Integer quantity;
}
