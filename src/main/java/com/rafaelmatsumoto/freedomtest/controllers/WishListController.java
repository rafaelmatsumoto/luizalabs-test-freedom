package com.rafaelmatsumoto.freedomtest.controllers;

import com.rafaelmatsumoto.freedomtest.models.ApiResponse;
import com.rafaelmatsumoto.freedomtest.models.Product;
import com.rafaelmatsumoto.freedomtest.models.WishList;
import com.rafaelmatsumoto.freedomtest.services.WishListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(path = "api/v1/wishlist")
public class WishListController {

    @Autowired
    private WishListService wishListService;

    @GetMapping("/{user_id}")
    public ResponseEntity<ApiResponse> getWishlist(@PathVariable("user_id") String userId) {
        WishList wishList = wishListService.findById(userId);
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setMessage("");
        if (wishList != null) {
            apiResponse.setStatus("Success");
            apiResponse.setData(wishList);
        } else {
            apiResponse.setStatus("Error");
        }
        return ResponseEntity.ok(apiResponse);
    }

    @GetMapping("/{user_id}/{product_id}")
    public ResponseEntity<ApiResponse> getProductInWishlist(@PathVariable("user_id") String userId,
                                                            @PathVariable("product_id") String productId) {
        Boolean result = wishListService.getProductInWishlist(userId, productId);
        ApiResponse apiResponse = new ApiResponse();
        if (result) {
            apiResponse.setMessage("Product found");
            apiResponse.setStatus("Success");
        } else {
            apiResponse.setMessage("Product not found");
            apiResponse.setStatus("Error");
        }
        return ResponseEntity.ok(apiResponse);
    }

    @PostMapping(
            path = "/{user_id}",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<ApiResponse> createWishlist(@PathVariable("user_id") String userId,
                                                      @RequestBody Product product) {
        String result = wishListService.save(userId, product);
        WishList wishList = wishListService.findById(userId);
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setData(wishList);
        apiResponse.setMessage(result);
        apiResponse.setStatus("Error");
        return ResponseEntity.ok(apiResponse);
    }

    @DeleteMapping("/{user_id}/{product_id}")
    public ResponseEntity<ApiResponse> deleteWishlist(@PathVariable("user_id") String userId,
                                                      @PathVariable("product_id") String productId) {
        Boolean result = wishListService.delete(userId, productId);
        ApiResponse apiResponse = new ApiResponse();
        if (result) {
            apiResponse.setStatus("Success");
            apiResponse.setMessage("Product sucessfully deleted");
        } else {
            apiResponse.setStatus("Error");
            apiResponse.setMessage("There was an error deleting the product");
        }
        return ResponseEntity.ok(apiResponse);
    }

}
