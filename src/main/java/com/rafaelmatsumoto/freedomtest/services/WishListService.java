package com.rafaelmatsumoto.freedomtest.services;

import com.rafaelmatsumoto.freedomtest.models.Product;
import com.rafaelmatsumoto.freedomtest.models.WishList;

public interface WishListService {
    String save(String userId, Product product);

    WishList findById(String userId);

    Boolean getProductInWishlist(String userId, String productId);

    Boolean delete(String userId, String productId);
}
