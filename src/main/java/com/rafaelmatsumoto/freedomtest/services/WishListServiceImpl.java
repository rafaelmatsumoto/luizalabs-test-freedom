package com.rafaelmatsumoto.freedomtest.services;

import com.rafaelmatsumoto.freedomtest.models.Product;
import com.rafaelmatsumoto.freedomtest.models.WishList;
import com.rafaelmatsumoto.freedomtest.repository.WishListRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WishListServiceImpl implements WishListService {

    @Autowired
    private WishListRepositoryImpl wishListRepository;

    @Override
    public String save(String userId, Product product) {
        return wishListRepository.save(userId, product);
    }

    @Override
    public WishList findById(String userId) {
        return wishListRepository.findById(userId);
    }

    @Override
    public Boolean getProductInWishlist(String userId, String productId) {
        return wishListRepository.getProductInWishlist(userId, productId);
    }

    @Override
    public Boolean delete(String userId, String productId) {
        return wishListRepository.delete(userId, productId);
    }
}
