package com.rafaelmatsumoto.freedomtest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class FreedomtestApplication {

	public static void main(String[] args) {
		SpringApplication.run(FreedomtestApplication.class, args);
	}

}

