package com.rafaelmatsumoto.freedomtest.repository;

import com.rafaelmatsumoto.freedomtest.models.Product;
import com.rafaelmatsumoto.freedomtest.models.WishList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class WishListRepositoryImpl implements WishListRepository {

    private static final String KEY = "WISHLIST";
    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public String save(String userId, Product product) {
        try {
            WishList wishList = (WishList) redisTemplate.opsForHash().get(KEY, userId);
            if (wishList != null) {
                ArrayList<Product> products = new ArrayList<Product>(wishList.getProducts().stream().toList());
                if (products.size() >= 20) {
                    return "List has reached full capacity";
                }
                products.add(product);
                wishList.setProducts(products);
                redisTemplate.opsForHash().put(KEY, userId, wishList);
            } else {
                WishList newWishList = new WishList();
                newWishList.setUserId(userId);
                newWishList.setProducts(List.of(product));
                redisTemplate.opsForHash().putIfAbsent(KEY, userId, newWishList);
            }
            return "Sucessfully added product " + product.getId() + " to the list";
        } catch (Exception e) {
            e.printStackTrace();
            return "There was an error while trying to add to the list " + e.getMessage();
        }
    }

    @Override
    public WishList findById(String userId) {
        try {
            return (WishList) redisTemplate.opsForHash().get(KEY, userId);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Boolean getProductInWishlist(String userId, String productId) {
        try {
            WishList wishList = (WishList) redisTemplate.opsForHash().get(KEY, userId);
            return wishList.getProducts().stream().anyMatch(product -> product.getId().equals(productId));
        } catch (Exception e) {
            e.printStackTrace();
            return Boolean.FALSE;
        }
    }

    @Override
    public Boolean delete(String userId, String productId) {
        try {
            WishList wishList = (WishList) redisTemplate.opsForHash().get(KEY, userId);
            List<Product> products = wishList.getProducts().stream().filter(product -> !product.getId().equals(productId))
                    .toList();
            wishList.setProducts(products);
            redisTemplate.opsForHash().put(KEY, wishList.getUserId(), wishList);
            return Boolean.TRUE;
        } catch (Exception e) {
            e.printStackTrace();
            return Boolean.FALSE;
        }
    }
}
