# LuizaLabs Test - Freedom

Aplicação para o desafio de código da LuizaLabs Freedom

## Requisitos

* Docker
* Docker Compose
* Java 18
* Maven

## Instalação

Utilizar o gerenciador [maven](https://maven.apache.org/).

```shell
mvn clean install
```

## Usage

* Comando para levantar o banco de dados Redis

```shell
docker-compose up -d
```

* Comando para rodar a aplicativo

```shell
mvn spring-boot:run
```

## Testing

```shell
mvn clean test
```
